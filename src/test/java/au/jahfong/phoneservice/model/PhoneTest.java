package au.jahfong.phoneservice.model;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class PhoneTest {

    @Test
    public void testGettersAndSetters() {
        // Given
        Phone phone = new Phone();
        LocalDateTime now = LocalDateTime.now();
        Customer customer = new Customer();

        phone.setId(1);
        phone.setPhoneNumber("0456822579");
        phone.setActivated(false);
        phone.setCreatedOn(now);
        phone.setUpdatedOn(now);
        phone.setCustomer(customer);

        // Then
        assertEquals(1, phone.getId());
        assertEquals("0456822579", phone.getPhoneNumber());
        assertFalse(phone.isActivated());
        assertEquals(now, phone.getCreatedOn());
        assertEquals(now, phone.getUpdatedOn());
        assertEquals(customer, phone.getCustomer());

    }
}