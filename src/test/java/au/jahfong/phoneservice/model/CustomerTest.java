package au.jahfong.phoneservice.model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class CustomerTest {

    @Test
    public void testGettersAndSetters() {
        // Given
        Customer customer = new Customer();
        List<Phone> phones = new ArrayList<>();

        customer.setId(1);
        customer.setReferenceNumber("REF-001");
        customer.setPhones(phones);

        // Then
        assertEquals(1, customer.getId());
        assertEquals("REF-001", customer.getReferenceNumber());
        assertEquals(phones, customer.getPhones());
    }
}