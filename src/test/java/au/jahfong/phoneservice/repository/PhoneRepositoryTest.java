package au.jahfong.phoneservice.repository;

import au.jahfong.phoneservice.model.Phone;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class PhoneRepositoryTest {

    @Autowired
    private PhoneRepository phoneRepository;

    @Test
    void should_find_By_Customer_ReferenceNumber() {
        List<Phone> found = phoneRepository.findByCustomer_ReferenceNumber("REF-001");

        assertEquals(2, found.size());
        assertEquals("0444444444", found.get(0).getPhoneNumber());
        assertEquals("0455555555", found.get(1).getPhoneNumber());
    }

    @Test
    void should_not_find_By_Customer_ReferenceNumber_without_phone() {
        List<Phone> found = phoneRepository.findByCustomer_ReferenceNumber("REF-002");

        assertTrue(found.isEmpty());
    }

    @Test
    void should_not_find_By_Customer_ReferenceNumber_that_doesnt_exist() {
        List<Phone> found = phoneRepository.findByCustomer_ReferenceNumber("REF-XXXX");

        assertTrue(found.isEmpty());
    }

    @Test
    void should_find_all() {
        List<Phone> found = phoneRepository.findAll();

        assertEquals(3, found.size());
        assertEquals("0444444444", found.get(0).getPhoneNumber());
        assertEquals("REF-001", found.get(0).getCustomer().getReferenceNumber());

        assertEquals("0455555555", found.get(1).getPhoneNumber());
        assertEquals("REF-001", found.get(1).getCustomer().getReferenceNumber());

        assertEquals("0466666666", found.get(2).getPhoneNumber());
        assertEquals("REF-003", found.get(2).getCustomer().getReferenceNumber());
    }

    @Test
    void should_find_by_phoneNumber() {
        Optional<Phone> found = phoneRepository.findByPhoneNumber("0444444444");
        assertEquals("0444444444", found.get().getPhoneNumber());
    }

    @Test
    void should_find_by_phoneNumber_return_null_for_not_found() {
        Optional<Phone> notFound = phoneRepository.findByPhoneNumber("XXXXXX");
        assertFalse(notFound.isPresent());
    }
}