package au.jahfong.phoneservice.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;

import au.jahfong.phoneservice.exception.PhoneNumberAlreadyActivated;
import au.jahfong.phoneservice.exception.PhoneNumberNotFound;
import au.jahfong.phoneservice.model.Customer;
import au.jahfong.phoneservice.model.Phone;
import au.jahfong.phoneservice.model.PhoneListRequest;
import au.jahfong.phoneservice.service.PhoneService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(PhoneController.class)
@ExtendWith(MockitoExtension.class)
class PhoneControllerTest {

    @MockBean
    private PhoneService phoneService;

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = createObjectMapper();

    private LocalDateTime now = LocalDateTime.now();

    @Test
    void should_get_all_phones() throws Exception {
        // Given
        Customer customer1 = Customer.builder().id(1).referenceNumber("REF-001").build();
        Customer customer2 = Customer.builder().id(2).referenceNumber("REF-002").build();

        Phone phone1 = createPhone(1, "0455555555", customer1);
        Phone phone2 = createPhone(2, "0455555566", customer1);
        Phone phone3 = createPhone(3, "0455555577", customer2);

        PhoneListRequest request = new PhoneListRequest();
        List<Phone> phones = Arrays.asList(phone1, phone2, phone3);
        when(phoneService.findAll(request)).thenReturn(phones);

        // When
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
            .get("/api/phones")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

        // Then
        JSONAssert.assertEquals(objectMapper.writeValueAsString(phones),
            result.getResponse().getContentAsString(), false);
    }


    @Test
    void should_activate_phone_number_successfully() throws Exception {
        // Given
        Customer customer1 = Customer.builder().id(1).referenceNumber("REF-001").build();
        Phone phone1 = createPhone(1, "0455555555", customer1);

        Phone activated = createPhone(1, "0455555555", customer1);
        activated.setActivated(true);

        when(phoneService.activate(phone1.getPhoneNumber())).thenReturn(activated);

        // When
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
            .post("/api/phones/0455555555/activation")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();

        // Then
        JSONAssert.assertEquals(objectMapper.writeValueAsString(activated),
            result.getResponse().getContentAsString(), false);
    }

    @Test
    void should_activate_phone_number_return_404_for_number_not_found() throws Exception {
        // Given
        Customer customer1 = Customer.builder().id(1).referenceNumber("REF-001").build();
        Phone phone1 = createPhone(1, "0455555555", customer1);

        doThrow(PhoneNumberNotFound.class).when(phoneService).activate(phone1.getPhoneNumber());

        // When
        mockMvc.perform(MockMvcRequestBuilders
            .post("/api/phones/0455555555/activation")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.errorCode", is("404")))
            .andExpect(jsonPath("$.errorMessage", is("The specified phone number does not exist.")));
    }

    @Test
    void should_activate_phone_number_return_400_for_number_already_activated() throws Exception {
        // Given
        Customer customer1 = Customer.builder().id(1).referenceNumber("REF-001").build();
        Phone phone1 = createPhone(1, "0455555555", customer1);

        doThrow(PhoneNumberAlreadyActivated.class).when(phoneService).activate(phone1.getPhoneNumber());

        // When
        mockMvc.perform(MockMvcRequestBuilders
            .post("/api/phones/0455555555/activation")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.errorCode", is("400")))
            .andExpect(jsonPath("$.errorMessage", is("The specified phone number is already activated.")));
    }

    Phone createPhone(int id, String phoneNumber, Customer customer) {
        return Phone.builder().id(id)
            .activated(false)
            .phoneNumber(phoneNumber)
            .createdOn(now).updatedOn(now)
            .customer(customer).build();
    }

    private ObjectMapper createObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }


}