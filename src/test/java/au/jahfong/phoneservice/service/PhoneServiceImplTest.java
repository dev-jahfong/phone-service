package au.jahfong.phoneservice.service;

import static org.junit.jupiter.api.Assertions.*;

import au.jahfong.phoneservice.exception.PhoneNumberAlreadyActivated;
import au.jahfong.phoneservice.exception.PhoneNumberNotFound;
import au.jahfong.phoneservice.model.Phone;
import au.jahfong.phoneservice.model.PhoneListRequest;
import au.jahfong.phoneservice.repository.PhoneRepository;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class PhoneServiceImplTest {

    @Autowired
    private PhoneRepository phoneRepository;

    private PhoneService phoneService;

    @BeforeEach
    void setup() {
        phoneService = new PhoneServiceImpl(phoneRepository, new PhoneSpecification());
    }

    @Test
    void should_findAll() {
        PhoneListRequest request = new PhoneListRequest();
        List<Phone> found = phoneService.findAll(request);

        assertEquals(3, found.size());
        assertEquals("0444444444", found.get(0).getPhoneNumber());
        assertEquals("REF-001", found.get(0).getCustomer().getReferenceNumber());

        assertEquals("0455555555", found.get(1).getPhoneNumber());
        assertEquals("REF-001", found.get(1).getCustomer().getReferenceNumber());

        assertEquals("0466666666", found.get(2).getPhoneNumber());
        assertEquals("REF-003", found.get(2).getCustomer().getReferenceNumber());
    }

    @Test
    void should_findAll_for_a_particular_customer_reference() {
        PhoneListRequest request = new PhoneListRequest();
        request.setCustomerReference("REF-001");

        List<Phone> found = phoneService.findAll(request);

        assertEquals(2, found.size());
        assertEquals("0444444444", found.get(0).getPhoneNumber());
        assertEquals("REF-001", found.get(0).getCustomer().getReferenceNumber());

        assertEquals("0455555555", found.get(1).getPhoneNumber());
        assertEquals("REF-001", found.get(1).getCustomer().getReferenceNumber());
    }

    @Test
    void should_findAll_return_empty_for_customer_reference_not_found() {
        PhoneListRequest request = new PhoneListRequest();
        request.setCustomerReference("REF-XXX");

        List<Phone> found = phoneService.findAll(request);

        assertTrue(found.isEmpty());
    }

    @Test
    void should_activate_phone_number() {
        Phone activated = phoneService.activate("0455555555");
        assertTrue(activated.isActivated());
    }

    @Test
    void should_activate_phone_number_throw_PhoneNumberAlreadyActivated() {
        assertThrows(PhoneNumberAlreadyActivated.class,
            () -> phoneService.activate("0444444444"));
    }

    @Test
    void should_activate_phone_number_throw_PhoneNumberNotFound() {
        assertThrows(PhoneNumberNotFound.class,
            () -> phoneService.activate("XXXXXXX"));
    }
}