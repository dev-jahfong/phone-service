package au.jahfong.phoneservice.controller;

import au.jahfong.phoneservice.model.Phone;
import au.jahfong.phoneservice.model.PhoneListRequest;
import au.jahfong.phoneservice.service.PhoneService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Defines the Phone REST API.
 */
@RestController
@RequestMapping("/api")
@Tag(name = "Phones API")
@Validated
@Slf4j
public class PhoneController {
    private final PhoneService phoneService;

    @Autowired
    public PhoneController(PhoneService phoneService) {
        this.phoneService = phoneService;
    }


    /**
     * Get all phones.
     * Accepting query params to filter the list {@link PhoneListRequest}
     *
     * @param request the parameter request
     * @return the list of phones with status 200
     */
    @Operation(summary = "Get All phones", responses = {
        @ApiResponse(responseCode = "200", description = "Successful")
    })
    @GetMapping("/phones")
    public ResponseEntity<List<Phone>> getAllPhones(@ParameterObject PhoneListRequest request) {
        log.debug("get all phones");
        List<Phone> phones = phoneService.findAll(request);

        return ResponseEntity.ok(phones);
    }

    /**
     * Activate a phone number.
     *
     * @param phoneNumber the phone number to activate
     * @return updated phone with status code 200 for successful activation,
     *         or status code 400 for already activated,
     *         or status code 404 code for phone number not found.
     */
    @Operation(summary = "Activate a phone", responses = {
        @ApiResponse(responseCode = "200", description = "Successful"),
        @ApiResponse(responseCode = "404", description = "Phone number not found"),
        @ApiResponse(responseCode = "400", description = "Phone number already activated")
    })
    @PostMapping("/phones/{phoneNumber}/activation")
    public ResponseEntity<Phone> activate(@PathVariable String phoneNumber) {
        log.debug("activate phone {}", phoneNumber);

        Phone activated = phoneService.activate(phoneNumber);

        return ResponseEntity.ok(activated);

    }

}
