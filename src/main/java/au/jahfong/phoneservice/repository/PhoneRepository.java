package au.jahfong.phoneservice.repository;

import au.jahfong.phoneservice.model.Phone;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * The JPA phone repository.
 */
public interface PhoneRepository
    extends JpaRepository<Phone, Long>, JpaSpecificationExecutor<Phone> {

    List<Phone> findByCustomer_ReferenceNumber(String referenceNumber);

    Optional<Phone> findByPhoneNumber(String phoneNumber);
}
