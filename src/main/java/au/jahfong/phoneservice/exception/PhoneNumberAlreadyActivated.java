package au.jahfong.phoneservice.exception;

/**
 * Exception for phone number already activated.
 */
public class PhoneNumberAlreadyActivated extends RuntimeException {

    public PhoneNumberAlreadyActivated(String phoneNumber) {
        super("Phone number is already activated: " + phoneNumber);
    }
}
