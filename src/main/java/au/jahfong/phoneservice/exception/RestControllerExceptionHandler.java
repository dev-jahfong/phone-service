package au.jahfong.phoneservice.exception;

import static au.jahfong.phoneservice.exception.ErrorCode.PHONE_NUMBER_ALREADY_ACTIVATED;
import static au.jahfong.phoneservice.exception.ErrorCode.PHONE_NUMBER_NOT_FOUND;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception Handler to handle all the exceptions thrown from the API.
 */
@RestControllerAdvice
@Slf4j
class RestControllerExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handles the {@link PhoneNumberNotFound}.
     *
     * @param ex the exception
     * @return an instance of {@link ErrorResponse} with {@code PHONE_NUMBER_NOT_FOUND}
     */
    @ExceptionHandler(PhoneNumberNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ErrorResponse handlePhoneNumberNotFound(PhoneNumberNotFound ex) {
        log.error("handlePhoneNumberNotFound", ex);

        return new ErrorResponse(PHONE_NUMBER_NOT_FOUND.value(),
            PHONE_NUMBER_NOT_FOUND.getReasonPhrase());
    }

    /**
     * Handles the {@link PhoneNumberAlreadyActivated}.
     *
     * @param ex the exception
     * @return an instance of {@link ErrorResponse} with {@code PHONE_NUMBER_ALREADY_ACTIVATED}
     */
    @ExceptionHandler(PhoneNumberAlreadyActivated.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ErrorResponse handlePhoneNumberAlreadyActivated(PhoneNumberAlreadyActivated ex) {
        log.error("PhoneNumberAlreadyActivated", ex);

        return new ErrorResponse(PHONE_NUMBER_ALREADY_ACTIVATED.value(),
            PHONE_NUMBER_ALREADY_ACTIVATED.getReasonPhrase());
    }

}
