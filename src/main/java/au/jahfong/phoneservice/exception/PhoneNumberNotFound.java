package au.jahfong.phoneservice.exception;

/**
 * Exception for phone number not found.
 */
public class PhoneNumberNotFound extends RuntimeException {

    public PhoneNumberNotFound(String phoneNumber) {
        super("Could not find phone number: " + phoneNumber);
    }
}
