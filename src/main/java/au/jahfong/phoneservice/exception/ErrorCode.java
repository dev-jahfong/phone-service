package au.jahfong.phoneservice.exception;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Defines the list of error codes.
 */
public enum ErrorCode {

    PHONE_NUMBER_NOT_FOUND("404", "The specified phone number does not exist."),
    PHONE_NUMBER_ALREADY_ACTIVATED("400", "The specified phone number is already activated.");

    private final String value;
    private final String reasonPhrase;

    ErrorCode(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    public String value() {
        return this.value;
    }

    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @JsonValue
    @Override
    public String toString() {
        return this.value + " " + this.reasonPhrase;
    }

}
