package au.jahfong.phoneservice.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * OpenAPI Configuration.
 */
@Configuration
public class OpenApiConfiguration {

    /**
     * The OpenApi bean definition.
     *
     * @param apiVersion          The api version.
     * @param apiName             The api name.
     * @param apiDescription      The api description.
     * @param apiDocumentationUrl The api external documentation url.
     * @return The OpenAPI object
     */
    @Bean
    public OpenAPI openApiConfig(@Value("${api.version}") String apiVersion,
                                 @Value("${api.name}") String apiName,
                                 @Value("${api.description}") String apiDescription,
                                 @Value("${api.documentationUrl}") String apiDocumentationUrl) {

        Info info = new Info().version(apiVersion).description(apiDescription).title(apiName);
        ExternalDocumentation documentation = new ExternalDocumentation()
            .description(apiName + " Documentation")
            .url(apiDocumentationUrl);

        return new OpenAPI().info(info).externalDocs(documentation);
    }
}
