package au.jahfong.phoneservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Containing the query parameters for phone list request.
 */
@Data
@NoArgsConstructor
public class PhoneListRequest {
    public String customerReference;
}
