package au.jahfong.phoneservice.service;

import au.jahfong.phoneservice.model.Phone;
import au.jahfong.phoneservice.model.PhoneListRequest;
import java.util.List;

/**
 * Phone Service.
 */
public interface PhoneService {

    List<Phone> findAll(PhoneListRequest phoneListRequest);

    Phone activate(String phoneNumber);
}
