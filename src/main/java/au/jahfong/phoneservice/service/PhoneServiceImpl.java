package au.jahfong.phoneservice.service;

import au.jahfong.phoneservice.exception.PhoneNumberAlreadyActivated;
import au.jahfong.phoneservice.exception.PhoneNumberNotFound;
import au.jahfong.phoneservice.model.Phone;
import au.jahfong.phoneservice.model.PhoneListRequest;
import au.jahfong.phoneservice.repository.PhoneRepository;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the Phone Service.
 */
@Service
public class PhoneServiceImpl implements PhoneService {
    private final PhoneRepository phoneRepository;
    private final PhoneSpecification phoneSpecification;

    @Autowired
    public PhoneServiceImpl(PhoneRepository phoneRepository,
                            PhoneSpecification phoneSpecification) {
        this.phoneRepository = phoneRepository;
        this.phoneSpecification = phoneSpecification;
    }

    @Override
    public List<Phone> findAll(PhoneListRequest phoneListRequest) {
        return phoneRepository.findAll(phoneSpecification.getSpecification(phoneListRequest));
    }

    @Override
    public Phone activate(String phoneNumber) {
        Phone phone = phoneRepository.findByPhoneNumber(phoneNumber)
            .orElseThrow(() -> new PhoneNumberNotFound(phoneNumber));

        if (phone.isActivated()) {
            throw new PhoneNumberAlreadyActivated(phoneNumber);
        }

        phone.setActivated(true);
        phone.setUpdatedOn(LocalDateTime.now());
        return phoneRepository.save(phone);
    }
}
