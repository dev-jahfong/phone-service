package au.jahfong.phoneservice.service;

import static org.springframework.data.jpa.domain.Specification.where;

import au.jahfong.phoneservice.model.Customer;
import au.jahfong.phoneservice.model.Phone;
import au.jahfong.phoneservice.model.PhoneListRequest;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

/**
 * THe JPA Specification for Phone.
 */
@Component
public class PhoneSpecification {

    /**
     * Build specification from the PhoneListRequest object.
     *
     * @param request the phoneListRequest
     * @return specification for phone
     */
    public Specification<Phone> getSpecification(PhoneListRequest request) {
        return (root, query, cb) -> {
            root.fetch("customer", JoinType.INNER);

            return where(customerAttributeEquals("referenceNumber", request.customerReference))
                .toPredicate(root, query, cb);
        };
    }

    private Specification<Phone> customerAttributeEquals(String attribute, String value) {
        return (root, query, cb) -> {
            if (value == null) {
                return null;
            }

            Path<Customer> customer = root.get("customer");
            return cb.equal(cb.lower(customer.get(attribute)), value.toLowerCase());
        };
    }

}
